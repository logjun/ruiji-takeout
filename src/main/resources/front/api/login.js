function loginApi(data) {
    return $axios({
      'url': '/user/login',
      'method': 'post',
      data
    })
  }
function sendCode(data) {
    return $axios({
        'url': '/user/checkCode',
        'method': 'post',
        data
    })
}

function loginoutApi() {
  return $axios({
    'url': '/user/logout',
    'method': 'post',
  })
}



  
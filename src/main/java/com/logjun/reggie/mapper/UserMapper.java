package com.logjun.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logjun.reggie.entity.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper extends BaseMapper<User>{
}

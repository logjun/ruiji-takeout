package com.logjun.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.logjun.reggie.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}

package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {

}

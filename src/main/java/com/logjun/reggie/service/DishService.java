package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.dto.DishDto;
import com.logjun.reggie.entity.Dish;

public interface DishService extends IService<Dish> {

    public void saveWithFlavor(DishDto dishDto);
    public DishDto getByIdWithFlavor(Long id);
    public void updateByIdWithFlavor(DishDto dishDto);
    public void delete(Long ids);
    public void updateWithStatus(Dish dish);
}

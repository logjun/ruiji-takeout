package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}

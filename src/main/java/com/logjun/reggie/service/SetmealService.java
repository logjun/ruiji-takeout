package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.dto.SetmealDto;
import com.logjun.reggie.entity.Setmeal;

public interface SetmealService extends IService<Setmeal> {
    public void saveWithDishes(SetmealDto setmealDto);
    public SetmealDto getByIdWithDishes(Long id);
    public void updateWithDishes(SetmealDto setmealDto);
    public void deleteWithDishes(Long id);
    public void updateWithStatus(Setmeal setmeal);
}

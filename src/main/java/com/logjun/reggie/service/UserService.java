package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.entity.Employee;
import com.logjun.reggie.entity.User;

public interface UserService extends IService<User> {
}

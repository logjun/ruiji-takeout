package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.entity.DishFlavor;

public interface DishFlavorService extends IService<DishFlavor> {
}

package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.entity.Employee;

public interface EmployeeService extends IService<Employee> {
}

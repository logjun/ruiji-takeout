package com.logjun.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logjun.reggie.common.CustomException;
import com.logjun.reggie.dto.SetmealDto;
import com.logjun.reggie.entity.Dish;
import com.logjun.reggie.entity.Setmeal;
import com.logjun.reggie.entity.SetmealDish;
import com.logjun.reggie.mapper.SetmealMapper;
import com.logjun.reggie.service.DishService;
import com.logjun.reggie.service.SetmealDishService;
import com.logjun.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper,Setmeal> implements SetmealService {

    @Autowired
    SetmealDishService setmealDishService;
    @Autowired
    DishService dishService;

    @Override
    @Transactional
    public void saveWithDishes(SetmealDto setmealDto) {
        this.save(setmealDto);
        //获取菜品信息
        List<SetmealDish> dishes = setmealDto.getSetmealDishes();
        dishes.stream().map(item -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(dishes);
    }

    @Override
    @Transactional
    public SetmealDto getByIdWithDishes(Long id) {
        SetmealDto setmealDto = new SetmealDto();
        Setmeal setmeal = this.getById(id);
        BeanUtils.copyProperties(setmeal,setmealDto);
        //构造条件查询器，根据套餐id查询对应的菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,id);
        List<SetmealDish> list = setmealDishService.list(queryWrapper);
        setmealDto.setSetmealDishes(list);
        return setmealDto;
    }

    @Override
    @Transactional
    public void updateWithDishes(SetmealDto setmealDto) {
        this.updateById(setmealDto);
        //获取菜品套餐信息
        List<SetmealDish> dishes = setmealDto.getSetmealDishes();
        //先删除套餐id关联的菜品，在添加新值进去
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,setmealDto.getId());
        setmealDishService.remove(queryWrapper);
        //添加新值，为每项菜品绑定套餐id
        dishes.stream().map(item ->{
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());
        setmealDishService.saveBatch(dishes);
    }

    @Override
    @Transactional
    public void deleteWithDishes(Long id) {
        this.removeById(id);
        //删除关联的套餐菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId,id);
        setmealDishService.remove(queryWrapper);
    }

    @Override
    @Transactional
    public void updateWithStatus(Setmeal setmeal) {
        //停售套餐后，对应的套餐菜品应该更改状态,起售套餐应判断关联的菜品停售
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        if(setmeal.getStatus() == 0){
            //如果套餐停售，套餐菜品也停售
            queryWrapper.eq(SetmealDish::getSetmealId,setmeal.getId());
            SetmealDish setmealDish = new SetmealDish();
            //1为停售，0为起售
            setmealDish.setStatus(0);
            setmealDishService.update(setmealDish,queryWrapper);
            this.updateById(setmeal);
            return;
        }//如果套餐起售，判断套餐中的菜品是否停售
        //根据套餐id获取对应的菜品id
        queryWrapper.eq(SetmealDish::getSetmealId,setmeal.getId());
        List<SetmealDish> list = setmealDishService.list();
        //遍历list，拿到每一个菜品id查询dish表，能查到停售状态的数据则不能起售
        for(SetmealDish setmealDish : list){
            LambdaQueryWrapper<Dish> queryWrapper1= new LambdaQueryWrapper<>();
            queryWrapper1.eq(Dish::getStatus,0).eq(Dish::getId,setmealDish.getDishId());
            if (dishService.count(queryWrapper1) > 0) {
                throw new CustomException("套餐存在停售的菜品。无法起售");
            }
        }
        this.updateById(setmeal);
    }
}

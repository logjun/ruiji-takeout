package com.logjun.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.logjun.reggie.common.CustomException;
import com.logjun.reggie.dto.DishDto;
import com.logjun.reggie.entity.Dish;
import com.logjun.reggie.entity.DishFlavor;
import com.logjun.reggie.entity.Setmeal;
import com.logjun.reggie.entity.SetmealDish;
import com.logjun.reggie.mapper.DishMapper;
import com.logjun.reggie.service.DishFlavorService;
import com.logjun.reggie.service.DishService;
import com.logjun.reggie.service.SetmealDishService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class DishServiceImpl extends ServiceImpl<DishMapper,Dish> implements DishService {

    @Autowired
    DishFlavorService dishFlavorService;
    @Autowired
    SetmealDishService setmealDishService;

    @Override
    @Transactional
    public void saveWithFlavor(DishDto dishDto) {
        //保存到菜品表
        this.save((Dish) dishDto);
        //获取菜品id
        Long id = dishDto.getId();
        //获取菜品口味列表，添加上菜品id值
        List<DishFlavor> dishFlavorList = dishDto.getFlavors().stream().map(item -> {
            item.setDishId(id);
            return item;
        }).collect(Collectors.toList());
        dishFlavorService.saveBatch(dishFlavorList);

    }

    @Override
    @Transactional
    public DishDto getByIdWithFlavor(Long id) {
        DishDto dishDto = new DishDto();
        Dish dish = this.getById(id);
        BeanUtils.copyProperties(dish,dishDto);
        //根据菜品id查询口味表
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dish.getId());
        List<DishFlavor> list = dishFlavorService.list(queryWrapper);
        dishDto.setFlavors(list);
        return dishDto;
    }

    @Override
    @Transactional
    public void updateByIdWithFlavor(DishDto dishDto) {
        //更新dish表字段
        this.updateById(dishDto);
        //获取菜品id查询到对应的flavor数据并删除
        LambdaQueryWrapper<DishFlavor> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(DishFlavor::getDishId,dishDto.getId());
        dishFlavorService.remove(queryWrapper);
        //将新的flavor数据更新到flavor表
        //为每条数据添加上菜品id
        List<DishFlavor> flavors = dishDto.getFlavors().stream().map(item ->{
            item.setDishId(dishDto.getId());
            return item;
        }).collect(Collectors.toList());
        dishFlavorService.saveBatch(flavors);
    }

    @Override
    @Transactional
    public void delete(Long ids) {
        //构造条件查询器
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getDishId,ids);
        //根据菜品id查询套餐菜品
        int count = setmealDishService.count(queryWrapper);
        //判断如果能查询到值，则不能删除
        if (count > 0){
            throw new CustomException("当前菜品关联了套餐，无法删除");
        }
        //删除菜品口味数据和菜品数据
        LambdaQueryWrapper<DishFlavor> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.eq(DishFlavor::getDishId,ids);
        dishFlavorService.remove(queryWrapper1);
        this.removeById(ids);

    }

    @Override
    @Transactional
    public void updateWithStatus(Dish dish) {
        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        LambdaQueryWrapper<SetmealDish> queryWrapper1 = new LambdaQueryWrapper<>();
        //如果菜品状态为1，则起售
        if (dish.getStatus() == 1){
            this.updateById(dish);
            //将套餐关联下的菜品也设置为起售状态
            queryWrapper1.eq(SetmealDish::getDishId, dish.getId());
            SetmealDish setmealDish = new SetmealDish();
            setmealDish.setStatus(1);
            setmealDishService.update(setmealDish,queryWrapper1);
            return;
        }//菜品状态为0，停售前判断关联套餐是否包含当前菜品
        queryWrapper1.eq(SetmealDish::getDishId,dish.getId()).eq(SetmealDish::getStatus,1);
        //如果能查到，则套餐关联了菜品，不能停售
        int count = setmealDishService.count(queryWrapper1);
        if (count > 0){
            throw new CustomException("当前菜品关联了套餐，不能停售");
        }
        this.updateById(dish);
    }
}

package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {

}

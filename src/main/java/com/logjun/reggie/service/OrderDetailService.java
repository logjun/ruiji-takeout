package com.logjun.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.logjun.reggie.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {

}

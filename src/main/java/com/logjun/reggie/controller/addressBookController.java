package com.logjun.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.logjun.reggie.common.BaseContext;
import com.logjun.reggie.common.R;
import com.logjun.reggie.entity.AddressBook;
import com.logjun.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/addressBook")
@Slf4j
public class addressBookController {
    @Autowired
    AddressBookService addressBookService;

    /**
     * 根据地址id查询地址信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @Cacheable(value = "addressCache",key = "'address' + #id")
    public R<AddressBook> get(@PathVariable Long id){
        AddressBook addressBook = addressBookService.getById(id);
        if (null != addressBook){
            return R.success(addressBook);
        }
        return R.error("没有找到该地址");
    }

    /**
     * 根据用户id获取全部地址信息
     * @return
     */
    @GetMapping("/list")
    @Cacheable(value = "addressCache",key = "'user'+'_'+'address'+'_'+'all'+#request.getSession().getAttribute('user')")
    public R<List<AddressBook>> list(HttpServletRequest request){
        //从本地线程池获取用户id
        Long userId = BaseContext.getCurrentId();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(null != userId, AddressBook::getUserId, userId);
        queryWrapper.orderByDesc(AddressBook::getUpdateTime);
        List<AddressBook> list = addressBookService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 新增地址
     * @param addressBook
     * @return
     */
    @PostMapping
    @CacheEvict(value = "addressCache",allEntries = true)
    public R<String> save(@RequestBody AddressBook addressBook){
        addressBook.setUserId(BaseContext.getCurrentId());
        log.info("addressBook:{}", addressBook);
        addressBookService.save(addressBook);
        return R.success("添加地址成功");
    }

    /**
     * 设置默认地址
     * @param addressBook
     * @return
     */
    @PutMapping("/default")
    @CacheEvict(value = "addressCache",allEntries = true)
    public R<String> setDefault(@RequestBody AddressBook addressBook){
        log.info("addressBook:{}", addressBook.toString());
        LambdaUpdateWrapper<AddressBook> wrapper = new LambdaUpdateWrapper<>();
        //根据用户id查询到所有的地址，将默认地址状态统一设置成0
        wrapper.eq(AddressBook::getUserId, BaseContext.getCurrentId());
        wrapper.set(AddressBook::getIsDefault, 0);
        addressBookService.update(wrapper);
        //将当前地址设置成默认
        addressBook.setIsDefault(1);
        addressBookService.updateById(addressBook);
        return R.success("更新成功");
    }

    /**
     * 修改地址信息
     * @param addressBook
     * @return
     */
    @PutMapping
    @CacheEvict(value = "addressCache",allEntries = true)
    public R<String> update(@RequestBody AddressBook addressBook){
        log.info("地址对象信息：{}",addressBook.toString());
        addressBookService.updateById(addressBook);
        return R.success("更新成功");
    }

    /**
     * 删除地址
     * @param ids
     * @return
     */
    @DeleteMapping
    @CacheEvict(value = "addressCache",allEntries = true)
    public R<String> delete(Long ids){
        addressBookService.removeById(ids);
        return R.success("删除成功");
    }

    /**
     * 获取默认地址
     * @return
     */
    @GetMapping("/default")
    @Cacheable(value = "addressDefaultCache",key = "'address'+'_'+'default'+#id")
    public R<AddressBook> get(){
        //获取当前用户id
        Long userId = BaseContext.getCurrentId();
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getIsDefault,1);
        queryWrapper.eq(AddressBook::getUserId,userId);
        AddressBook addressBook = addressBookService.getOne(queryWrapper);
        return R.success(addressBook);
    }

}

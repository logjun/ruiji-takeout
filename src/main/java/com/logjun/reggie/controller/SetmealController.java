package com.logjun.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.logjun.reggie.common.R;
import com.logjun.reggie.dto.DishDto;
import com.logjun.reggie.dto.SetmealDto;
import com.logjun.reggie.entity.Category;
import com.logjun.reggie.entity.Dish;
import com.logjun.reggie.entity.Setmeal;
import com.logjun.reggie.entity.SetmealDish;
import com.logjun.reggie.service.CategoryService;
import com.logjun.reggie.service.SetmealDishService;
import com.logjun.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {

    @Autowired
    SetmealService setmealService;
    @Autowired
    CategoryService categoryService;
    @Autowired
    SetmealDishService setmealDishService;

    /**
     * 套餐信息分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @Cacheable(value = "setmealCache",key = "'setmealPage'+#page+'_'+#pageSize+'_'+#name")
    public R Page(int page,int pageSize, String name){
        log.info("进入到套餐分页查询方法");
        Page<Setmeal> stemealPage = new Page<>(page,pageSize);
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(name!= null,Setmeal::getName,name);
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        setmealService.page(stemealPage,queryWrapper);
        //创建setmealDto实体对象，用来封转返回给前端的信息
        List<Setmeal> records = stemealPage.getRecords().stream().map(item ->{
            LambdaQueryWrapper<SetmealDish> queryWrapper1 = new LambdaQueryWrapper<>();
            SetmealDto setmealDto = new SetmealDto();
            BeanUtils.copyProperties(item,setmealDto);
            //获取分类id查询分类表
            Long categoryId = item.getCategoryId();
            Category category = categoryService.getById(categoryId);
            if (category != null){
                setmealDto.setCategoryName(category.getName());
            }
            //根据套餐id查询关联的菜品信息
            Long id = item.getId();
            queryWrapper1.eq(SetmealDish::getSetmealId,id);
            List<SetmealDish> list = setmealDishService.list(queryWrapper1);
            if (list.size() > 0){
                setmealDto.setSetmealDishes(list);
            }
            return setmealDto;
        }).collect(Collectors.toList());
        stemealPage.setRecords(records);
        return R.success(stemealPage);
    }

    /**
     * 新增套餐信息
     * @param setmealDto
     * @return
     */
    @PostMapping
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> save(@RequestBody SetmealDto setmealDto){
        //新增套餐，提示将套餐中的菜品保存到setmealDish表
        setmealService.saveWithDishes(setmealDto);
        return R.success("新增成功");
    }

    /**
     * 根据套餐id获取对应信息回显
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @Cacheable(value = "setmealCache",key = "'setmeal'+#id")
    public R<SetmealDto> get(@PathVariable long id){
        //查询setmeal表和套餐对应的菜品
        SetmealDto setmealDto = setmealService.getByIdWithDishes(id);
        return R.success(setmealDto);
    }

    /**
     * 更新套餐数据
     * @param setmealDto
     * @return
     */
    @PutMapping
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> update(@RequestBody SetmealDto setmealDto){
        //更新setmeal表和对应的setmealDish数据
        setmealService.updateWithDishes(setmealDto);
        return R.success("更新成功");
    }

    /**
     * 删除套餐操作
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    @CacheEvict(value = "setmealCache",allEntries = true,beforeInvocation = false)
    public R<String> delete(@RequestParam List<Long> ids) {
        for (Long id : ids) {
            //删除套餐信息和关联的套餐菜品信息
            setmealService.deleteWithDishes(id);
        }
        return R.success("删除成功");
    }

    /**
     * 更新套餐状态
     *
     * @param ids
     * @param status
     * @return
     */
    @PostMapping("/status/{status}")
    @CacheEvict(value = "setmealCache",allEntries = true)
    public R<String> updateStatus(@RequestParam List<Long> ids, @PathVariable Integer status) {
        for (Long id : ids) {
            Setmeal setmeal = new Setmeal();
            setmeal.setId(id);
            setmeal.setStatus(status);
            //如果当前菜品关联了套餐则不能更改状态
            setmealService.updateWithStatus(setmeal);
        }
        if (status == 1) {
            return R.success("已起售");
        }
        return R.success("已停售");
    }

    /**
     *根据分类id查询套餐数据
     * @param setmeal
     * @return
     */
    @GetMapping("/list")
    @Cacheable(value = "setmealCache",key = "'setmeal_categoryId'+#setmeal.getCategoryId()+'_'+1")
    public R<List<Setmeal>> list(Setmeal setmeal){
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Setmeal::getStatus, 1);
        queryWrapper.eq(null != setmeal.getCategoryId(),Setmeal::getCategoryId,setmeal.getCategoryId());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        List<Setmeal> list = setmealService.list(queryWrapper);
        return R.success(list);
    }

}

package com.logjun.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.logjun.reggie.common.BaseContext;
import com.logjun.reggie.common.R;
import com.logjun.reggie.entity.ShoppingCart;
import com.logjun.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@RestController
@Slf4j
@RequestMapping("/shoppingCart")
public class shoppingCarController {

    @Autowired
    ShoppingCartService shoppingCartService;

    @GetMapping("/list")
    @Cacheable(value = "shoppingCartCache", key = "'shoppingCartList'+#request.getSession().getAttribute('user')")
    public R list(HttpServletRequest request){
        log.info("查看购物车...");

        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, BaseContext.getCurrentId());
        queryWrapper.orderByAsc(ShoppingCart::getCreateTime);

        List<ShoppingCart> list = shoppingCartService.list(queryWrapper);
        return R.success(list);
    }

    /**
     * 添加菜品至购物车
     * @return
     */
    @PostMapping("/add")
    @CacheEvict(value = "shoppingCartCache", allEntries = true)
    public R<ShoppingCart> save(@RequestBody ShoppingCart shoppingCart){
        //获取当前用户id
        Long id = BaseContext.getCurrentId();
        shoppingCart.setUserId(id);
        Long userId = shoppingCart.getUserId();
        //设置查询条件
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,userId);
        //判断是菜品还是套餐
        if (null != shoppingCart.getDishId()){
            queryWrapper.eq(ShoppingCart::getDishId,shoppingCart.getDishId());
        }else {
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }
        //查询，如果能查询到数据，在原有数据上+1
        ShoppingCart one = shoppingCartService.getOne(queryWrapper);
        if (null != one){
            one.setNumber(one.getNumber() + 1);
            shoppingCartService.updateById(one);
        }else{
            //查不到数据，添加至购物车，数量默认为1
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartService.save(shoppingCart);
            one = shoppingCart;
        }
        return R.success(one);
    }

    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    @CacheEvict(value = "shoppingCartCache", allEntries = true)
    public R<String> clean(){
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());
        shoppingCartService.remove(queryWrapper);
        return R.success("删除成功");
    }

    /**
     * 更新购物车数量操作
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    @CacheEvict(value = "shoppingCartCache", allEntries = true)
    public R<ShoppingCart> subCart(@RequestBody ShoppingCart shoppingCart){
        //获取当前用户id
        Long currentId = BaseContext.getCurrentId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId,currentId);
        //判断是菜品还是套餐
        if (null != shoppingCart.getDishId()){
            queryWrapper.eq(ShoppingCart::getDishId,shoppingCart.getDishId());
        }else{
            queryWrapper.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }
        shoppingCart.setUserId(currentId);
        shoppingCart.setCreateTime(LocalDateTime.now());
        ShoppingCart one = shoppingCartService.getOne(queryWrapper);
        one.setNumber(one.getNumber() - 1);
        //如果购物车的该菜品数量>1，数量减一，只有一份是删除购物车数据
        if (one.getNumber() == 1){
            shoppingCartService.removeById(one);
        }
        shoppingCartService.updateById(one);
        return R.success(one);
    }
}

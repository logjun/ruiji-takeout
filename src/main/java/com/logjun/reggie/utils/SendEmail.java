package com.logjun.reggie.utils;

import com.logjun.reggie.common.CustomException;

import javax.mail.*;
import javax.mail.internet.*;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class SendEmail {

    /**
     * 发送邮件(参数自己根据自己的需求来修改，发送短信验证码可以直接套用这个模板)
     *
     * @param from_email 发送人的邮箱
     * @param pwd        发送人的授权码
     * @param recevices  接收人的邮箱
     * @param code       验证码
     * @return
     */

    private static String sender = "2096997724@qq.com";
    private static String pwd = "ekivfrdtupugecib";      //授权码

    public static void sendEmail(String email,String code)throws Exception{
        Properties properties = new Properties();
        properties.put("mail.transport.protocol", "smtp");// 连接协议
        properties.put("mail.smtp.host", "smtp.qq.com");// 主机名
        properties.put("mail.smtp.port", 465);// 端口号
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.ssl.enable", "true");// 设置是否使用ssl安全连接 ---一般都使用
        properties.put("mail.debug", "true");// 设置是否显示debug信息 true 会在控制台显示相关信息
        // 得到回话对象
        Session session = Session.getInstance(properties);
            // 获取邮件对象
            Message message = new MimeMessage(session);
            // 设置发件人邮箱地址
            message.setFrom(new InternetAddress(sender));
            // 设置收件人邮箱地址
            message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(email)});
            //message.setRecipient(Message.RecipientType.TO, new InternetAddress("xxx@qq.com"));//一个收件人
            // 设置邮件标题
            message.setSubject("验证码:");
            // 设置邮件内容

            message.setText("您的验证码为："+code+"。五分钟内有效");//这是我们的邮件要发送的信息内容
            // 得到邮差对象
            Transport transport = session.getTransport();
            // 连接自己的邮箱账户
            transport.connect(sender, pwd);// 密码为QQ邮箱开通的stmp服务后得到的客户端授权码,输入自己的即可
            // 发送邮件
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();
    }
}

package com.logjun.reggie.dto;

import com.logjun.reggie.entity.OrderDetail;
import com.logjun.reggie.entity.Orders;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class OrdersDto extends Orders implements Serializable {
    //订单明细
    private List<OrderDetail> orderDetails;
    //收货人
    private String consignee;
}

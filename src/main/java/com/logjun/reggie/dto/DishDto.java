package com.logjun.reggie.dto;

import com.logjun.reggie.entity.Dish;
import com.logjun.reggie.entity.DishFlavor;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 自定义实体类，用来封转返回给前端的数据
 */
@Data
public class DishDto extends Dish implements Serializable {

    //菜品口味
    private List<DishFlavor> Flavors;
    //所属分类名
    private String categoryName;
    //
    private Integer copies;

}

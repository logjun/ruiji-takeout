package com.logjun.reggie.dto;

import com.logjun.reggie.entity.Setmeal;
import com.logjun.reggie.entity.SetmealDish;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class SetmealDto extends Setmeal implements Serializable {
    private List<SetmealDish> setmealDishes;
    private String categoryName;
}

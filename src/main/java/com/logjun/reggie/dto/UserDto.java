package com.logjun.reggie.dto;

import com.logjun.reggie.entity.User;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto extends User implements Serializable {
    private String code;
}
